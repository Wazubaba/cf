module util.xdgspec;

import std.process: environment;
import std.string: format;
import std.array: split;

/**
	Simple implementation of xdg basedir specification.

	If you specify version as `Warnings` then a message
	will be printed out if env vars are not specified.
**/

private void warn(string var, string defaultSetting)
{
	version (Warnings)
	{
		import std.stdio:writeln;
		writeln("Warning, cannot find the %s environment var, this should probably be set to %s".format(var, defaultSetting));
	}
}

/// Returns the home directory of the current user. Throws "Cannot find home directory" on error
string getHomeDir()
{
	string result = environment.get("HOME");

	// If we cannot find the home directory at all, something fishy is going on...
	if (result == "") throw(new Exception("Cannot find home directory"));
	return result;
}

/// Returns the XDG_DATA_HOME directory
string getDataHome()
{
	string result = environment.get("XDG_DATA_HOME", "");

	if (result == "")
	{
		warn("XDG_DATA_HOME", "$HOME/.local/share");
		result = "%s/.local/share".format(getHomeDir());
	}
	return result;
}

/// Returns the XDG_CONFIG_HOME directory
string getConfigHome()
{
	string result = environment.get("XDG_CONFIG_HOME");

	if (result == "")
	{
		warn("XDF_CONFIG_HOME", "$HOME/.config");
		result = "%s/.config".format(getHomeDir());
	}

	return result;
}

/// Returns the XDG_DATA_DIRS list
string getDataDirs()
{
	string result = environment.get("XDG_DATA_DIRS", "");

	if (result == "")
	{
		warn("XDG_DATA_DIRS", "/usr/local/share/:/usr/share/");
		result = "/usr/local/share/:/usr/share/";
	}
	return result;
}

/// Returns the XDG_DATA_DIRS list as a string array
string[] getDataDirsAsArray()
{
	string[] result;
	string dirs = getDataDirs();

	foreach(string dir; split(dirs, ":"))
		result ~= dir;

	return result;
}


/// Returns the XDG_CONFIG_DIRS list
string getConfigDirs()
{
	string result = environment.get("XDG_CONFIG_DIRS", "");

	if (result == "")
	{
		warn("XDG_CONFIG_DIRS", "/etc/xdg");
		result = "/etc/xdg";
	}
	return result;
}

/// Returns the XDG_CONFIG_DIRS list as a string array
string[] getConfigDirsAsArray()
{
	string[] result;
	string dirs = getConfigDirs();

	foreach(string dir; split(dirs, ":"))
		result ~= dir;

	return result;
}

/// Returns the XDG_CACHE_HOME dir
string getCacheHome()
{
	string result = environment.get("XDG_CACHE_HOME", "");

	if (result == "")
	{
		warn("XDG_CACHE_HOME", "$HOME/.cache");
		result = "%s/.cache".format(getHomeDir());
	}
	return result;
}

/// Returns the XDG_RUNTIME_DIR
string getRuntimeDir()
{
	/*
		This one is a bit confusing. There is no good standardized place but a
		hell of a lot of rules about it. Quick duckduckgo search resulted in
		suggestion to just use /tmp, despite it not meeting the criteria
		completely. As a 100% meeting of this standard would require extra
		effort on the part of the end-user, it has been decided to just point
		to /tmp since that directory is typically guarenteed to exist on most
		standard installations of linux, and meets a few of the criteria
		already.
	*/
	return "/tmp/";
}
