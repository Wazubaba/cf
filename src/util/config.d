module util.config;

/// Exception thrown when an invalid path is given
class InvalidPath: Exception
{
	@safe nothrow this(string path)
	{
		super("'" ~ path ~ "' does not exist or cannot be loaded!");
	}
}

/// Exception thrown when parsing fails
class ParserError: Exception
{
	@safe nothrow this(string path, string error)
	{
		super("Error parsing config from " ~ path ~ ": " ~ error);
	}

	@safe nothrow this(string error)
	{
		super(error);
	}
}

/// Type for a Key
enum KeyType
{
	STRING,
	NUMBER,
	BOOL,
	NULL,
}

/// A container for a given entry in a config
struct Key
{
	string name; /// Name of the entry
	string value; /// Value of the entry
	KeyType type = KeyType.NULL; /// Type of the entry
}

/// Test if a key is null or not
bool isNull(Key key)
{
	if (key.type == KeyType.NULL) return true;
	return false;
}

alias Config = Key[];

/// Load a target file into a Key array
Config load(string path)
{
	import std.file: exists, isFile, read, write;
	import std.conv: to;

	Config rhs;

	if (path.exists && path.isFile)
	{
		auto data = to!string(read(path));

		try rhs = parse(data);
		catch (ParserError e) throw new ParserError(path, e.msg);
	}
	else throw new InvalidPath(path);

	return rhs;
}

/// Parse data into a Key array
Config parse(string data)
{
	import std.string: lineSplitter, split, indexOf, strip;
	import core.exception: RangeError;

	Config rhs;

	size_t i;
	foreach (line; data.lineSplitter())
	{
		i++; // GG dlang, you broke foreach loops handling an inc var now \o/
		try
		{
			if (line.strip.length == 0) continue;
			else
			if (line.strip[0] == '#') continue;
			else
			{
				Key newKey;
				newKey.name = line.split("=")[0].strip;
				newKey.value = line[line.indexOf('=')+1..$].strip;
				newKey.type = KeyType.STRING;

				rhs ~= newKey;
			}
		}
		catch (RangeError e)
		{
			import std.string: format;
			throw new ParserError("Error on line %s".format(i));
		}
	}

	return rhs;
}

void save(Config cfg)
{
	import std.stdio: File;
	import util.helpers: getConfigPath;
	import std.string: format, strip;

	auto fp = File(getConfigPath(), "w");
	foreach (key; cfg)
		version (Posix) fp.write("%s = %s\n".format(key.name.strip, key.value.strip));
		version (Windows) fp.write("%s = %s\r\n".format(key.name.strip, key.value.strip));
	fp.close();
}

/// Get a key by a given name
Key getKey(Config cfg, string keyName)
{
	foreach (key; cfg)
		if (key.name == keyName) return key;
	return Key.init;
}

/// Add a key to the config avoiding duplication
bool addKey(ref Config cfg, string name, string value, KeyType type)
{
	if (cfg.getKey(name).isNull)
	{
		Key newKey = {name, value, type};
		cfg ~= newKey;
		return true;
	}
	return false;
}

/// Remove a key from the config
bool removeKey(ref Config cfg, string name)
{
	import std.algorithm.mutation: remove;
	if (cfg.length == 1)
	{
		cfg.length = 0;
		return true;
	} else
	if (cfg.length == 0)
		return false;

	foreach (i, key; cfg)
	{
		if (key.name == name)
		{
			cfg = cfg.remove(i);
			return true;
		}
	}
	return false;
}

unittest
{
	import std.stdio: writefln;
	auto cfg = parse("ohaider = nya\nraaaaaaaaaaawr           = meow5");

	foreach (key; cfg)
		writefln("Key: %s\nValue: %s\nType: %s", key.name, key.value, key.type);
}
