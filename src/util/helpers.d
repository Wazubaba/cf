module util.helpers;

import std.file: exists, isFile;
import std.path: buildPath;

import util.xdgspec;

bool existsAndIsFile(string path)
{
	if (path.exists() && path.isFile())
		return true;
	return false;
}

void generateConfigPath()
{
	import std.path: pathSplitter;
	import std.array: array;
	import std.file: mkdirRecurse, write;

	string configPath = getConfigPath();

	if (configPath.existsAndIsFile) return;

	// Generate path
	string targetPath = buildPath(configPath.pathSplitter().array[0..$-1]);
	mkdirRecurse(targetPath);

	// Generate empty file
	write(configPath, "");
}

string getConfigPath()
{
	if (".cfrc".existsAndIsFile())
		return ".cfrc";
	else
	{
		string xdgpath = buildPath(getConfigHome(), "cf", "cfrc");
		string homepath = buildPath(getHomeDir(), ".cfrc");
		if (xdgpath.existsAndIsFile())
			return xdgpath;
		else
		if (homepath.existsAndIsFile())
			return homepath;
		else
			return xdgpath;
	}
}

unittest
{
	generateConfigPath();
}
