import std.stdio;
import std.path;
import std.file;

import util.config;
import util.helpers;

import dwst.argparse;

enum VERSION = "0.1.0";

void showHelp()
{
	writeln(
"Usage: cf [OPTION] BOOKMARK [PATH]
Change into the directory denoted by BOOKMARK if it exists.

Mandatory arguments to long options are mandatory for short options too.
\t-a, --add BOOKMARK [PATH]    create a new BOOKMARK that points to PATH
\t-d, --delete BOOKMARK    delete BOOKMARK
\t-h, --help    display this help and exit
\t-l, --list    show all bookmarks
\t-s, --show BOOKMARK   output path associated with BOOKMARK and exit
\t-v, --version    output version information and exit

Examples:
\tcf --add homefolder ~/
\tcf --delete homefolder #Disclaimer: this only deletes the keyword association");
}

int main(string[] args)
{
	Opts opts;
	bool argHelp, argAdd, argDel, argList, argVersion, argShow;
	string[] subArgAdd, subArgDel, subArgShow;
	opts.register("-a", "--add", &argAdd, 2LU, &subArgAdd);
	opts.register("-d", "--delete", &argDel, 1LU, &subArgDel);
	opts.register("-h", "--help", &argHelp);
	opts.register("-l", "--list", &argList);
	opts.register("-s", "--show", &argShow, 1LU, &subArgShow);
	opts.register("-v", "--version", &argVersion);

	try args.parse(opts);
	catch (NotEnoughSubArgs e)
	{
		if (e.msg == "-a")
			writeln("cf: option requires two arguments -- '-a'");
		else
			writefln("cf: option requires an argument -- '%s'");
		writeln("Try 'cf --help' for more information.");
		return -1;
	}

	if (argHelp)
	{
		showHelp();
		return 0;
	} else
	if (argVersion)
	{
		writeln(VERSION);
		return 0;
	}

	Config cfg;

	try cfg = load(getConfigPath());
	catch (InvalidPath e)
	{
		generateConfigPath();
	}

	if (argList)
	{
		foreach (key; cfg)
			writefln("%s: %s", key.name, key.value);
		return 0;
	} else
	if (argAdd)
	{
		import std.path: asAbsolutePath;
		import std.array: array; // Why...
		import std.file: exists, isDir;
		string targetDir = subArgAdd[1].asAbsolutePath().array();

		if (!targetDir.exists || !targetDir.isDir)
		{
			writefln("Target directory '%s' does not exist", subArgAdd[1]);
			return -1;
		}

		if (!cfg.addKey(subArgAdd[0], subArgAdd[1].asAbsolutePath().array(), KeyType.STRING))
			writefln("Bookmark '%s' already exists", subArgAdd[0]);
			cfg.save();
		return 0;
	} else
	if (argDel)
	{
		if (!cfg.removeKey(subArgDel[0]))
		{
			writefln("No bookmark named '%s' exists.", subArgDel[0]);
		}
		cfg.save();
		return 0;
	} else
	if (argShow)
	{
		auto bookmark = cfg.getKey(subArgShow[0]);
		if (bookmark.isNull)
			writefln("No bookmark named '%s' exists.", subArgShow[0]);
		else
			writefln("Bookmark '%s': %s", subArgShow[0], bookmark.value);
		return 0;
	}

	if (args.length > 1)
	{
		if (args[1][0] == '-')
		{
			writefln("cf: Invalid argument '%s'\nTry 'cf --help' for more information.", args[1]);
			return 0;
		}

		auto key = cfg.getKey(args[1]);
		if (!key.isNull) write(key.value);
		return 10;
	}
	return 5;
}
