cf(){
	if [ $# -lt 1 ]; then
		echo "cf: missing operand"
		echo "Try 'cf --help' for more information."
		return
	fi

	targDir=`cf_bookmarks $@`
	result=$?

	if [ "$targDir" != "" ]; then
		if [ $result = 10 ]; then
			cd "$targDir"
		elif [ $result = 5 ]; then
			echo "No entry for '$1' found"
		else
			echo "$targDir"
		fi
	fi
}
