// D import file generated from 'src/argparse.d'
module wst.argparse;
nothrow pure @safe class NotEnoughSubArgs : Exception
{
	this(string message);
}
struct argSet
{
	string sArg;
	string lArg;
	bool* flag;
	size_t numArgs;
	string[]* subArgs;
}
struct Opts
{
	argSet[] args;
	void register(string sArg, string lArg, bool* flag, size_t numArgs = 0, string[]* subArgs = null);
}
void parse(string[] args, Opts options);
