DC= dmd
BIN= cf_bookmarks

ifeq ($(shell uname -m), x86_64)
#	LIBPATH= -L-Llin64
	LIBFILES= $(shell find support/lin64 -type f -name "*.a")
else
	# Assume we are on 32bit or something
#	LIBPATH= -L-Llin32
	LIBFILES= $(shell find support/lin32 -type f -name "*.a")
endif

# Allow overriding of the arch
ifeq ($(ARCH), 32)
#	LIBPATH= -L-Llin32
	LIBFILES= $(shell find support/lin32 -type f -name "*.a")
endif

ifeq ($(ARCH), 64)
#	LIBPATH= -L-Llin64
	LIBFILES= $(shell find support/lin64 -type f -name "*.a")
endif

SRCFILES= $(shell find src -type f ! -name "*skip.*" -and ! -path "*skip.*" -name "*.d")
#LIBFILES= $(shell find lib -type f -name "*.a")
OBJFILES= $(patsubst %.d, .bldfiles/%.o, $(SRCFILES))

all: $(BIN)

.bldfiles/%.o: %.d
	$(DC) $(DFLAGS) -Isrc -Iinclude -c -of$@ $<

$(BIN): $(OBJFILES)
	$(DC) $(DFLAGS) $(LIBPATH) -of$@ $(OBJFILES) $(LIBFILES)

install: $(BIN)
	mkdir -p $(DESTDIR)/etc/profile.d
	cp src/cf-shell.sh $(DESTDIR)/etc/profile.d/.
	chmod -x $(DESTDIR)/etc/profile.d/cf-shell.sh
	mkdir -p $(DESTDIR)/usr/bin
	cp cf_bookmarks $(DESTDIR)/usr/bin/.
	echo "cf is now installed, you will need to source"
	echo "$(DESTDIR)/etc/profile.d/cf-shell.sh in your .bashrc for it to work."

uninstall:
	rm $(DESTDIR)/etc/profile.d/cf-shell.sh
	rm $(DESTDIR)/usr/bin/cf_bookmarks
	echo "cf is now uninstalled, you will need to restart your open shells"
	echo "before the function ceases to exist :)"

clean:
	-@$(RM) $(BIN)
	-@$(RM) -r .bldfiles
