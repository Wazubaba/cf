#!/bin/bash

_cf ()
{
	local cur
	COMPREPLY=()
	cur=${COMP_WORDS[COMP_CWORD]}
	prev=${COMP_WORDS[COMP_CWORD-1]}
	opts="-h --help -l --list -v --version -s --show -a --add -d --delete"

	if [[ ${cur} == -* ]]; then
		COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
		return 0;
	fi

	local bookmarks=$(sed "s/ =.*//" ~/.config/cf/cfrc)
	COMPREPLY=( $(compgen -W "${opts} ${bookmarks}" -- ${cur}) )
}

complete -F _cf cf

