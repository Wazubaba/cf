# C F

## Info
This tool acts sort of like the shell-builtin `cd`, only it works with bookmarked
directories which you specify.

For example, `cf -s mybookmark ~/mycode/someProject/ohaideru` will save a bookmark
to (by default, `~/.config/cf/cfrc`) and from then on you can execute `cf mybookmark`
to be changed to that directory. Note that cf will properly expand the target path
to an absolute path if possible, even for symlinks.

## Usage
See `cf --help` for more information.
An important note: cf is a combination of two files, a shell-function that you need
to source, and a binary which is controlled by the shell function.

## Compiling
Just issue `make` to build. The makefile has both `install` and `uninstall` rules
as well.

## Installation
Installation, untill I can work out how to get the bash shell to automatically source
a given function (I had thought this was what /etc/profile.d was for.. but hey... -_-)
this will unfortunately be a three-step procedure.

1) execute `sudo make install`

2) add
    ```
	if [ -f /etc/profile.d/cf-shell.sh ]; then
		. /etc/profile.d/cf-shell.sh
	fi
	```
to your .bashrc file

3) either re-source your .bashrc or reopen your terminals
